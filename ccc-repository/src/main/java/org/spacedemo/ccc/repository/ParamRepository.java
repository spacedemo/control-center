package org.spacedemo.ccc.repository;

import org.spacedemo.ccc.model.Param;
import org.springframework.data.repository.CrudRepository;

public interface ParamRepository extends CrudRepository<Param, String> {

    public Param findByIdentifier(String identifier);

}
