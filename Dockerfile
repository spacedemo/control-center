FROM openjdk:8-jre-slim

COPY ccc-app/target/ccc-app.jar .

CMD ["java", "-jar", "ccc-app.jar"]
