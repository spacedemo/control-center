package org.spacedemo.ccc.satellite;

import java.util.List;

import org.spacedemo.ccc.controller.ParamController;
import org.spacedemo.ccc.model.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping({ "/param" })
public class ParamService {

	@Autowired
	private ParamController paramController;

	@GetMapping
	public List<Param> list() {
		return paramController.list();

	}

	@PostMapping
	public Param create(@RequestBody Param Satellite) {
		return paramController.create(Satellite);
	}

	@GetMapping(value = "/{id}")
	public Param get(@PathVariable String id) {
		return paramController.get(id);
	}

	@PutMapping(value = "/{id}")
	public Param update(@PathVariable String id, @RequestBody Param Satellite) {
		return paramController.update(id, Satellite);
	}

	@DeleteMapping(value = "/{id}")
	public Param delete(@PathVariable String id) {
		return paramController.delete(id);
	}

}
