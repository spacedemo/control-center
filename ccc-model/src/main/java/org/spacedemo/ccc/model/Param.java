package org.spacedemo.ccc.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Param {

	@Id
	private String identifier;
	private String value;
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
