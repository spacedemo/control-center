package org.spacedemo.ccc.controller;

import java.util.ArrayList;
import java.util.List;
import org.spacedemo.ccc.model.Param;
import org.spacedemo.ccc.repository.ParamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ParamController {

	@Autowired
	private ParamRepository paramRepository;

	public List<Param> list() {
		List<Param> list = new ArrayList<Param>();
		paramRepository.findAll().forEach(list::add);
		return list;
	}

	public Param create(Param satellite) {
		return paramRepository.save(satellite);
	}

	public Param get(String id) {
		return paramRepository.findById(id).orElse(null);
	}

	public Param update(String id, Param satellite) {
		return paramRepository.save(satellite);
	}

	public Param delete(String id) {
		Param sat = paramRepository.findById(id).orElse(null);
		if (sat != null)
			paramRepository.deleteById(id);
		return sat;
	}

}
